#ifndef VARIABLE
#define VARIABLE

#include <stdio.h>
#include <stdlib.h>

size_t fsize(FILE* f);

#endif