#include <stdio.h>
#include "fileio.h"

int main(int argc, char* argv[])
{
    // Open File
    FILE* f = fopen(argv[1], "r");
    const size_t size = fsize(f);
    
    char* fc = malloc(size);
    printf("%zu\n", size);
    fread (fc, 1, size, f);
    printf("%s\n", fc);
    
    free(fc);
    fclose(f);
    return 0;
}