CC = clang-7
OBJ := fileio.o aaroncat.o
CFLAGS := 
DEPS = fileio.h

all: aaroncat

%.o: %.c 
	$(CC) -c -o $@ $< $(CFLAGS)

aaroncat: $(OBJ)
	$(CC) -o aaroncat $^ 


    
