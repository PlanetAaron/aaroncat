#include "fileio.h"

size_t fsize(FILE* f)
{
    size_t s;
    fseek(f, 0, SEEK_END);
    s = ftell(f);
    fseek(f, 0, SEEK_SET);
    return s;
}